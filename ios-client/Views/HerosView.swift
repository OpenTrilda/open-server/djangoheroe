//
//  ContentView.swift
//  Heros
//
//  Created by Paul Plaquette on 23/04/2021.
//

import Foundation
import SwiftUI


// A view that shows the data for one Restaurant.
struct HeroeRow: View {
    var heroe: Heroe

    var body: some View {
        VStack (alignment: .leading){
            Text("\(heroe.id)")

            Text("\(heroe.name)")
            Text("\(heroe.alias)")
        }
    }
}


struct HeroesView: View {

    @EnvironmentObject var heroesData : HeroesDataModel

   
    //print (herosData)

    //@State private var herosData = [Hero]()

    /*
    let heros = [
        Hero(id: 1, name: "Wonder Woman", alias: "Dianna Prince"),
        Hero(id: 2, name: "Batman", alias: "Bruce Wayne"),
        Hero(id: 3, name: "Superman", alias: "Clark Kent"),
    ]
 */


    var body: some View {

        VStack(alignment: .leading) {
            Text("Heros lookup")
                .padding()

            List(heroesData.heroes,  id: \.self) { heroe in
                    HeroeRow(heroe: heroe)
            }
            .onAppear(perform: loadListOnAppear)
        }
    }

    func loadHeros() {
        // URLComponents is building an "escaped" text url
        // most of components used could go in constant string (let)
        // for example in a URL builder file.

        // http://127.0.0.1:8000/heros/?format=api

        var urlMaker = URLComponents()
        urlMaker.scheme = "http"
        urlMaker.host = "localhost"
        urlMaker.port = 8000;
        urlMaker.path = "/heroes/"
        urlMaker.queryItems = [
            URLQueryItem(name: "format", value: "json"),
        ]

        print (urlMaker.url!)
        guard let url = urlMaker.url
        else {
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"


        let defaultSession = URLSession(configuration: .default)

        var dataTask: URLSessionDataTask?

        dataTask = defaultSession.dataTask(with: request) { data, response, error in

        print(data)
        print(response)
        print(error)

         if let data = data,
            let response = response as? HTTPURLResponse,
            response.statusCode == 200 {

                if let decodedResponse = try? JSONDecoder().decode([Heroe].self, from: data) {

                    print (decodedResponse)

                    // we have good data – go back to the main thread
                    DispatchQueue.main.async {
                        // update our UI
                        self.heroesData.heroes = decodedResponse
                        print (data)
                    }

                    // everything is good, so we can exit
                    return
                }
            }
        }
        dataTask?.resume()
    }

    func loadListOnAppear() {
        loadHeros()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HeroesView()
                .environmentObject(HeroesDataModel())
        }
    }
}
