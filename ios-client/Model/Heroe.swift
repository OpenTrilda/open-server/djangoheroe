//
//  Hero.swift
//  Hero data
//
//  Created by Paul Plaquette on 23/04/2021.
//

import Foundation

struct Heroe:  Codable, Hashable , Equatable  {

    var id: Int                               // mandatory. uniqueID that identifies a suggestion

    var name: String                         // mandatory. country name

    var alias: String                       // mandatory supported types
}
