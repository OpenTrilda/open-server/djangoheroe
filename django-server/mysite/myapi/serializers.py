# serializers.py
from rest_framework import serializers

from .models import Heroe

class HeroeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Heroe
        fields = ( 'id', 'name', 'alias')
