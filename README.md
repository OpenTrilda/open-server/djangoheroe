# DjangoHeroe


# Status

sample on halt
should be continued asap

# Requirements
a simple sample that rquires
- python3 (installed with homebrew on a mac)
- an iOS Swift UI plus Combine client
- an SQlite3 database editor 

# Content

the server in Django to serve a Full REST API
the subject are super heroes and their "human" alter ego

allows a **sandbox** for django,a (may be) full REST API, swift UI, combine for iOS
and later compose and flow for Android



#how to run
 cd mysite
 python3 manage.py runserver

 Open Heros.xcode.proj
 it is connected to the localhost django server if you see more than 3 heroes

 note that "héros"(heros) is "heroes" in French

# TODO
- an Android version with kotlin, compose is planned...
- pave the way for a really Full REST API , 
it is not the case at this time:
  - add more actions  CRUD, 
    - currenly only the get to show items in a list is implement
    - (create, delete, update) on both client and server side 
    have to be implemented
  - manage more errors on both sides
- add more details
- add a details view
- add test
- and UI Test on both platform
- how to test a djnago ?


